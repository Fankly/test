import axios from "axios";

const axios_instace = axios.create({
  baseURL: process.env.VUE_APP_URL,
  timeout: 10000,
  headers: {
    "content-type": "application/x-www-form-urlencoded",
  },
});

// request拦截器
axios_instace.interceptors.request.use(
  (config) => {
    return config;
  },
  (err) => {
    return Promise.reject(err);
  }
);

// respone拦截器
axios_instace.interceptors.response.use(
  (response) => {
    return response;
  },
  (err) => {
    if (err && err.response) {
      switch (err.response) {
        case 400:
          err.message = "错误请求";
          break;
        case 401:
          err.message = "未授权,请重新登录";
          break;
        case 403:
          err.message = "拒绝访问";
          break;
        case 404:
          err.message = "请求错误,未找到该资源";
          break;
        case 405:
          err.message = "请求方法未允许";
          break;
        case 408:
          err.message = "请求超时";
          break;
        case 500:
          err.message = "服务器端出错";
          break;
        default:
          err.message = `连接错误${err.response.status}`;
      }
    } else {
      err.message = "连接到服务器失败";
    }
    return Promise.resolve(err.response);
  }
);

export default axios_instace;
